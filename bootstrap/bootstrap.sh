#!/bin/bash

[ ! -n "$DEBUG" ] || set -x

set -ue

function onerr {
    echo 'Cleaning up after error...'
    exit -1
}
trap onerr ERR


# Yes, the Vagrant ansible_local provisioner has the ability to install Ansible for us but this gives us more control
# over the result. 
function ansible_install {
    if ! apt-cache policy | grep -i ansible | grep -i ppa ; then
	echo "Installing Ansible ppa..."
	(set -x;
	 apt-add-repository -y ppa:ansible/ansible)
    fi

    if ! command -vp ansible > /dev/null 2>&1 ; then
	echo "Installing Ansible..."
	(set -x;
	 DEBIAN_FRONTEND=noninteractive apt-get update && \
	     apt-get install -y ansible)
    fi
}


# FIXME: move this to Ansible.
function inspec_install () {
    echo "${FUNCNAME[0]}..."
    if ! command -v inspec > /dev/null 2>&1 ; then
	(set -x;
	 wget -c -q -O /vagrant/cache/inspec-install.sh https://omnitruck.chef.io/install.sh && \
	     chmod +x /vagrant/cache/inspec-install.sh && \
	     /vagrant/cache/inspec-install.sh -d /vagrant/cache -P inspec)
    fi
}


function main () {
    ansible_install
    inspec_install
}


main
