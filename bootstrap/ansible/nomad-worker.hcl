datacenter = "local-vagrant"
data_dir = "/opt/nomad"
bind_addr = "{{GetAllInterfaces | include \"network\" \"192.168.56.0/24\" | attr \"address\"}}"

server {
       enabled = false
}

advertise {
	  http = "{{GetAllInterfaces | include \"network\" \"192.168.56.0/24\" | attr \"address\"}}"
	  rpc  = "{{GetAllInterfaces | include \"network\" \"192.168.56.0/24\" | attr \"address\"}}"
	  serf = "{{GetAllInterfaces | include \"network\" \"192.168.56.0/24\" | attr \"address\"}}"
}

client {
       enabled = true
       
       cni_path = "/usr/lib/cni"

       # This is the weirdo VirtualBox routing interface (gw -> 10.0.2.2/32).
       # Let's create a host_network name for it just for completeness.
       host_network "virtualbox" {
       		    cidr = "10.0.2.15/24"
		    reserved_ports = "22,2222"
       }

       # In multi-node Vagrant environments using NAT, this is the VMs second interface and
       # interface it can use to talk to other VMs behind the NAT.
       host_network "public" {
       		    cidr = "192.168.56.0/24"
		    reserved_ports = "22,222"
       }      
}

plugin "docker" {
       config {
       	      gc {
	      	 image_delay = "168h" # 7 days
	      }	      
       }      
}

consul {
       address = "localhost:8500"
}