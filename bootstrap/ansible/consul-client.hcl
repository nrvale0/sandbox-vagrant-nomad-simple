datacenter = "local-vagrant"
data_dir = "/opt/consul"
ui = true
server = false
bind_addr = "{{GetAllInterfaces | include \"network\" \"192.168.56.0/24\" | attr \"address\"}}"
retry_join = ["consul0"]