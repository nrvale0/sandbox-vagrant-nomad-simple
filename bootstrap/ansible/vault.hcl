ui = true
listener "tcp" {
	 tls_disable = "true"
	 address = "{{GetAllInterfaces | include \"network\" \"192.168.56.0/24\" | attr \"address\"}}:8200"
}

storage "file" {
	path = "/var/vault/data"
}
