datacenter = "local-vagrant"
data_dir = "/opt/nomad"
bind_addr = "{{GetAllInterfaces | include \"network\" \"192.168.56.0/24\" | attr \"address\"}}"

server {
       enabled = true
       bootstrap_expect = 1
}

advertise {
	  http = "{{GetAllInterfaces | include \"network\" \"192.168.56.0/24\" | attr \"address\"}}"
	  rpc  = "{{GetAllInterfaces | include \"network\" \"192.168.56.0/24\" | attr \"address\"}}"
	  serf = "{{GetAllInterfaces | include \"network\" \"192.168.56.0/24\" | attr \"address\"}}"
}

client {
       enabled = false
}

consul {
       address = "localhost:8500"
}