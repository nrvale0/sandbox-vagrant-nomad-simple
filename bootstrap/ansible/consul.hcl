datacenter = "local-vagrant"
data_dir = "/opt/consul"
ui = true
server = true
bootstrap_expect = 1
bind_addr = "{{GetAllInterfaces | include \"network\" \"192.168.56.0/24\" | attr \"address\"}}"
addresses {
	  http = "{{GetAllInterfaces | include \"network\" \"192.168.56.0/24\" | attr \"address\" }}"
}