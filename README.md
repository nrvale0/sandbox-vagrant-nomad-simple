# sandbox-vagrant-nomad-simple


## Description

A simple Nomad sandbox.

-   Consul and Nomad control planes consisting of a single node each.
-   A Nomad Client/worker with Docker
    -   Nomad worker does not currently support nested virtualization so no QEMU/KVM/etc for the time being.


## Usage


### Requirements

This environment has been developed and tested with the following requirements:

    - OS: Ubuntu 22.10
    - Vagrant: 2.3.4
    - VirtualBox: 7.0.6r155176
    - Ansible: core 2.14.1
    
    - Vagrant plugins:
    vagrant-auto_network (1.0.3)
    vagrant-hosts (2.9.0)
    vagrant-vbguest (0.31.0)


### Operation

```
$ vagrant up && \ # (with a fair amount of waiting)
vagrant snapshot save fresh

.. <stuff> ..

$ vagrant suspend
$ vagrant up && \
vagrant provision --provision-with validate
```


### Networking

There are no Vagrant-managed port-forwards in the environment. This is mostly because of the peculiar way that Virtualbox does private networking where all of the VMs in the private network will share an interface+IP address as their first interface and yet also have a unique interface+IP address. Consul and Nomad are configured to bind to the **unique** interface and address on each node.

To connect to the Consul management UI:

```
$ vagrant ssh consul0 -- -f -L 8500:localhost:8500
$ xdg-open http://localhost:8500
```

Similarly, to connect to the Nomad management UI:

```
$ vagrant ssh nomad0 -- -f -L <desired host port>:$(vagrant hosts list | grep nomad0 | cut -f1 -d' '):4646
$ xdg-open http://localhost:4646    
```

Note that this also means that apps deployed into the Nomad worker will **not** be reachable outside of the Virtualbox private network unless you do a similar port-mapping. The port for the deployed service will be dynamic depending on scheduling.

```
$ vagrant ssh worker0 -- -f -L <desired host port>:$(vagrant hosts list | grep worker0 | cut -f1 -d' '):<scheduled worker port>
$ xdg-open http://localhost:<desired host port>
```

You can configure port-forwards in the VirtualBox UI if you prefer but keep in mind that those changes may or may not persist across resets of the environment depending on how friendly Vagrant and VirtualBox are feeling with each other on that particular day. They have a "turbulent" relationship as it is.


### Additonal Notes

-   No TLS and no ACLs

    This is intended as an environment for some basic science projects using Consul + Nomad (including Enterprise). No TLS and no ACLs because I need to be able to sniff traffic and play around with API and do so quickly.
    
    For more production-like lab environment, hit me up with ask and I'll point you elsewhere.

-   Nested virtualization

    Nested virt is probably the next big push for support in the environment. It's quite tricky with Virtualbox though and it may only work for Vagrant+VirtualBox+Linux (and not OSX). We'll see how that shakes out.


## Contact

-   nrvale0@protonmail.com | nathan@hashicorp.com