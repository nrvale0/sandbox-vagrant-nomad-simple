#!/bin/bash

[ ! -n "$DEBUG" ] || set -x

set -ue


function inspec_install () {
    echo "${FUNCNAME[0]}..."
    if ! command -v inspec > /dev/null 2>&1 ; then
	(set -x;
	 wget -c -q -O /vagrant/cache/inspec-install.sh https://omnitruck.chef.io/install.sh && \
	     chmod +x /vagrant/cache/inspec-install.sh && \
	     /vagrant/cache/inspec-install.sh -d /vagrant/cache -P inspec)
    fi
}


function inspec_run () {
    echo "${FUNCNAME[0]}..."
    (set -x; 
     inspec detect --chef-license=accept-silent && \
	 inspec exec /vagrant/roles/nomad/validate.d/inspec.d/)
}


function main () {
    inspec_install
    inspec_run
}


main
