ip = %x(networkctl status enp0s8 | grep "  Address" | cut -d':' -f2 | xargs).strip

describe service('nomad') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

describe port(4646) do
  it { should be_listening }
  its('addresses') { should include "#{ip}" }
end

