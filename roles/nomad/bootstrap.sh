#!/bin/bash

[ ! -n "$DEBUG" ] || set -x

set -eux


# various common definitions and functions
. /vagrant/roles/common.sh


function consul_install () {
    echo "${FUNCNAME[0]}..."
    if ! command -v consul > /dev/null 2>&1 ; then
	if test -f /vagrant/roles/consul/enterprise.lic ; then
	    (set -x;
	     apt-get install -y consul-enterprise)
	else
	    (set -x;
	     apt-get install -y consul)
	fi
    fi
}


function nomad_install () {
    echo "${FUNCNAME[0]}..."
    if ! command -v nomad > /dev/null 2>&1 ; then
	if test -f /vagrant/roles/nomad/enterprise.lic ; then
	    (set -x;
	     apt-get install -y nomad-enterprise)
	else
	    (set -x;
	     apt-get install -y nomad)
	fi
    fi
}


function consul_config () {
    echo "${FUNCNAME[0]}..."    
    cat <<EOF > /etc/consul.d/consul.hcl

    if test -f /vagrant/roles/consul/enterprise.lic ; then
	(set -x;
	 cp /vagrant/roles/consul/enterprise.lic /etc/consul.d/ && \
	 cat <<EOF > /etc/consul.d/enterprise.hcl
license_path = "/etc/consul.d/enterprise.lic"
EOF
	)
    fi
}


function nomad_config () {
    echo "${FUNCNAME[0]}..."

    # if license file exists then create config for license
    if test -f /vagrant/roles/nomad/enterprise.lic ; then
	(set -x;
	 cp /vagrant/roles/nomad/enterprise.lic /etc/nomad.d/ && \
	 cat <<EOF > /etc/nomad.d/enterprise.hcl
server {
       license_path = "/etc/nomad.d/enterprise.lic"
}
EOF
	)
    fi

    # if NOMAD_SIMPLE_VAULT is set then we'll extract a root key and feed it to the Nomad control plane nodes
    set +u
    if [[ -z "${NOMAD_SIMPLE_VAULT}" ]] ; then
	echo "Configuring Nomad with a Vault token..."
	local root_token="$(jq -r .root_token /vagrant/cache/vault/init-info.json)"
	cat <<EOF > /etc/nomad.d/vault.hcl
vault {
      enabled = true
      address = "http://vault0:8200"
      token = "${root_token}"
}
EOF
    fi
    set -u
}


function consul_run () {
    echo "${FUNCNAME[0]}..."
    (set -x;
     systemctl enable consul && \
	 systemctl stop consul && \
	 systemctl start consul)
}


function nomad_run () {
    echo "${FUNCNAME[0]}..."
    (set -x;
     systemctl enable nomad && \
	 systemctl stop nomad && \
	 systemctl start nomad)
}


function main () {
    prep
    consul_install
    consul_config
    consul_run
    nomad_install
    nomad_config
    nomad_run
}

main
