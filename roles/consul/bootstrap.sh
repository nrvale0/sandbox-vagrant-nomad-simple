#!/bin/bash

[ ! -n "$DEBUG" ] || set -x

set -eux


# various common definitions and functions
. /vagrant/roles/common.sh


function consul_install () {
    echo "${FUNCNAME[0]}..."

    if ! command -v consul > /dev/null 2>&1 ; then
	if test -f /vagrant/roles/consul/enterprise.lic ; then
	    (set -x;
	     apt-get install -y consul-enterprise)
	else
	    (set -x;
	     apt-get install -y consul)
	fi
    fi
}


function consul_config () {
    echo "${FUNCNAME[0]}..."    

    if test -f /vagrant/roles/consul/enterprise.lic ; then
	(set -x;
	 cp /vagrant/roles/consul/enterprise.lic /opt/consul/ && \
	 cat <<EOF > /etc/consul.d/enterprise.hcl
license_path = "/opt/consul/enterprise.lic"
EOF
	 )
    fi
}


function consul_run () {
    echo "${FUNCNAME[0]}..."
    (set -x;
     systemctl enable consul && \
	 systemctl stop consul && \
	 systemctl start consul)
}


function main () {
    prep
    consul_install
    consul_config
    consul_run
}

main
