function prep () {
    echo "${FUNCNAME[0]}..."

    (set -x;
     systemctl disable apt-daily.timer)

    if ! grep 'apt.releases.hashicorp.com' /etc/apt/sources.list ; then
	(set -x;
	 curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add - && \
	     apt-add-repository -y "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main")
    fi
    
    export DEBIAN_FRONTEND=noninteractive

    if ! command -v jq > /dev/null 2>&1 ; then
	(set -x;
	 apt-get update
	 apt-get install -y curl wget httpie jq lsb-release)
    fi
}
