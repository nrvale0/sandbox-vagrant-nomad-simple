describe service('nomad') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

describe package('containers-storage') do
  it { should be_installed }
end

describe package('containernetworking-plugins') do
  it { should be_installed }
end

