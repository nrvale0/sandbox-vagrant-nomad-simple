describe package ('podman') do
  it { should be_installed }
end

#############################
# these things for rootless #
#############################
describe package ('slirp4netns') do
  it { should be_installed }
end

describe package ('fuse-overlayfs') do
  it { should be_installed }
end

describe file('/etc/subuid') do
  its('content') { should match /vagrant/ }
end

describe file('/etc/subgid') do
  its('content') { should match /vagrant/ }
end
