#!/bin/bash

[ ! -n "$DEBUG" ] || set -x

set -eux


# various common definitions and functions
. /vagrant/roles/common.sh


function consul_install () {
    echo "${FUNCNAME[0]}..."
    if ! command -v consul > /dev/null 2>&1 ; then
	if test -f /vagrant/roles/consul/enterprise.lic ; then
	    (set -x;
	     apt-get install -y consul-enterprise)
	else
	    (set -x;
	     apt-get install -y consul)
	fi
    fi
}


function nomad_install () {
    echo "${FUNCNAME[0]}..."
    if ! command -v nomad > /dev/null 2>&1 ; then
	if test -f /vagrant/roles/nomad/enterprise.lic ; then
	    (set -x;
	     apt-get install -y nomad-enterprise)
	else
	    (set -x;
	     apt-get install -y nomad)
	fi
    fi
}


function nomad_config () {
    echo "${FUNCNAME[0]}..."    

    # if NOMAD_SIMPLE_VAULT is set then add Vault integration
    set +u
    if [[ -z "${NOMAD_SIMPLE_VAULT}" ]] ; then
	echo "Configuring Nomad for Vault..."
	cat <<EOF > /etc/nomad.d/vault.hcl
vault {
      enabled = true
      address = "http://vault0:8200"
}	    
EOF
    fi
    set -u
}


function consul_run () {
    echo "${FUNCNAME[0]}..."
    (set -x;
     systemctl enable consul && \
	 systemctl stop consul && \
	 systemctl start consul)
}


function nomad_run () {
    echo "${FUNCNAME[0]}..."
    (set -x;
     systemctl enable nomad && \
	 systemctl stop nomad && \
	 systemctl start nomad)
}


function docker_install () {
    echo "${FUNCNAME[0]}..."
    if ! command -v docker > /dev/null 2>&1 ; then
	(set -x;
	 apt-get install -y docker.io && \
	     systemctl enable --now docker)
    fi
}


function cncf_install () {
    echo "${FUNCNAME[0]}..."
    if [[ ! -x /usr/lib/cni/loopback ]] ; then
	(set -x;
	 apt-get install -y containernetworking-plugins containers-storage)
    fi
}


function podman_install () {
    echo "${FUNCNAME[0]}..."
    if ! command -v podman > /dev/null 2>&1 ; then
	(set -x;
	 apt-get install -y podman)
    fi
}


function main () {
    prep
    cncf_install
    docker_install
    podman_install
    consul_install
    consul_config
    consul_run
    nomad_install
    nomad_config
    nomad_run
}

main
