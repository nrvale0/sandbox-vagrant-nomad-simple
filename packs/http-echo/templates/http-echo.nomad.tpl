job "http-echo" {
  datacenters = [[ .my.datacenters | toStringList ]]

  group "echo" {
    count = [[ .my.count ]]

    network {
      mode = "bridge"
      port "http" {
	host_network = "public" # host network by this name defined in the client config
	static = [[ .my.listen_port | toString | quote ]]
      }
    }

    task  "server" {
      driver = "docker"
      config {
	image = "hashicorp/http-echo:latest"
	args = [
	  "-listen", ":[[ .my.listen_port | toString ]]",
	  "-text", "[[ .my.message | toString ]]",
	]
	ports = [ "http" ]
      }
    }

    service {
      tags = [ "http-echo", "nomad-pack" ]
      port = "http"

      check {
	name = "tcp"
	type = "tcp"
	port = "http"
	interval = "10s"
	timeout = "2s"
      }

      check {
	type = "http"
	port = "http"
	path = "/"
	interval = "10s"
	timeout = "2s"

	check_restart {
	  limit = 3
	  grace = "90s"
	  ignore_warnings = false
	}
      }
    }
    
    # A bit aggressive for a restart block but that is because this pack is for devetest + validation purposes and
    # thus has scenarios with many failures and restarts.
    restart {
      attempts = 100
      delay = "5s"
      interval = "10m"
      mode = "fail"
    }   
  }

  # This has no material impact on the Job. It's just here to test map merges via -var and --var-file.
  # meta {
  #      merged_map = [[ .my.merge_test ]]
  # }
}
