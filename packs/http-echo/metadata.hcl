app {
  url = "https://gitlab.com/nrvale0/sandbox-vagrant-nomad-simple"
  author = "Nathan Valentine"
}

pack {
  name = "http-echo"
  description = "Set a string to be echoed to requestor via HTTP."
  url = "https://gitlab.com/nrvale0/sandbox-vagrant-nomad-simple"
  version = "0.0.1"
}
