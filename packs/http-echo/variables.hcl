variable "datacenters" {
  type = list(string)
  default = [ "local-vagrant" ]
}

variable "listen_port" {
  type = string
  default = "8080"
}

variable "message" {
  type = string
  default = "Hello from the http_echo Nomad Pack!"
}

variable "count" {
  type = string
  default = 1
}

# This has no material impact on the templates. It's simply here for testing map merge via --var and --var-file.
variable "merge_test" {
  type = map(string)
  default = {
    "one" = 1
    "two" = 2
  }
}
