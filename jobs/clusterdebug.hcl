job "clusterdebug" {
  datacenters = [ "local-vagrant" ]
  type = "service"
 
  # update {
  #   healthy_deadline = "30s"
  #   progress_deadline = "45s"
  # }

  group "shell" {

    network {
      mode = "bridge"
      port "nc" {
	static = "8888"
	host_network = "public" # host network by this name defined in client config
      }
    }

    task "clusterdebug-bridged" {
      driver = "docker"
      config {
	image = "nrvale0/clusterdebug"
	command = "/bin/bash"
	args = [ "-c", "while true; do date | nc -l -p 8888; done" ]
	ports = [ "nc" ]
      }
      
      resources {
	memory = 512
      }
    }
  }
}
