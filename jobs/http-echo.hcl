job "http-echo" {
  datacenters = [ "local-vagrant" ]

  group "echo" {
    count = 1

    network {
      mode = "bridge"
      port "http" {
	host_network = "public" # host network by this name defined in the client config
	static = "8080"
      }
    }

    task  "server" {
      driver = "docker"
      config {
	image = "hashicorp/http-echo:latest"
	args = [
	  "-listen", ":8080",
	  "-text", "Hello and welcome."
	]
	ports = [ "http" ]
      }
    }
  }

}
