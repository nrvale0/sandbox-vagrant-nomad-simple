AutoNetwork.default_pool = '192.168.56.0/21'

Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/jammy64"

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
  end

  config.vm.provision "ansible" do |a|
    a.groups = {
      "vault_servers"  => ["vault[0:2:1]"],
      "consul_servers" => ["consul[0:4:1]"],
      "nomad_servers"  => ["nomad[0:4:1]"],
      "vault_clients"  => [],
      "consul_clients" => [],
      "nomad_clients"  => ["worker[0:9:1]"]
    }
    a.inventory_path = "bootstrap/ansible/inventory/"
    a.playbook = "bootstrap/ansible/playbook.yml"
  end

  # At some point, going to want to enable nested virt in the Nomad workers.
  # config.vm.provider "virtualbox" do |vb|
  #   vb.customize ["modifyvm", :id, "--nested-hw-virt", "on"]
  # end
 
  #############################################################################
  # Note: There's definitely some code compression that could happen below -- #
  # ex: loop through a dictionary with params for each role type --           #
  # however I'm leaving the code uncompressed in case I find something about  #
  # one of the roles which requires a special provisoning step etc.           #
  #############################################################################

  ########################
  # Consul control plane #
  ########################l
  config.vm.define "consul0" do |c|
    c.vm.hostname = "consul0"
    c.vm.network :private_network, :auto_network => true
    c.vm.provision :hosts, :autoconfigure => true, :sync_hosts => true
    c.vm.provision "bootstrap", type: "ansible", playbook: "bootstrap/ansible/playbook.yml", run: "always"
    c.vm.provision "validate", type: "shell", keep_color: false, run: "always", path: "roles/consul/validate.sh"
  end

  ###############################
  # Nomad control plane node(s) #
  ###############################
  config.vm.define "nomad0" do |n|
    n.vm.hostname = "nomad0"
    n.vm.network :private_network, :auto_network => true
    n.vm.provision :hosts, :autoconfigure => true, :sync_hosts => true
    n.vm.provision "bootstrap", type: "ansible", playbook: "bootstrap/ansible/playbook.yml", run: "always"
    n.vm.provision "validate", type: "shell", keep_color: false, run: "always", path: "roles/nomad/validate.sh"
  end
  
  ########################
  # Nomad worker node(s) #
  ########################
  worker_count = ENV.key?('NOMAD_SIMPLE_WORKERS') ? ENV['NOMAD_SIMPLE_WORKERS'].to_i : 1
  (0..(worker_count -1)).each { |n|
    config.vm.define "worker#{n}" do |w|
      w.vm.hostname = "worker#{n}"
      w.vm.network :private_network, :auto_network => true
      w.vm.provision :hosts, :autoconfigure => true, :sync_hosts => true
      w.vm.provision "bootstrap", type: "ansible", playbook: "bootstrap/ansible/playbook.yml", run: "always"
      w.vm.provision "validate", type: "shell", keep_color: false, run: "always", path: "roles/worker/validate.sh"
    end
  }
end
